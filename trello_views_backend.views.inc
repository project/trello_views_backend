<?php

/**
 * Implements hook_views_data().
 */
function trello_views_backend_views_data() {
  $data = array();

  $data['trello']['table']['group'] = t('Trello');

  $data['trello']['table']['base'] = array(
    'title' => t('Trello'),
    'help' => t('Queries Trello.'),
    'query class' => 'trello_views_backend',
  );

  $data['trello']['name'] = array(
    'title' => t('Name'),
    'help' => t('Name of the item'),
    'field' => array(
      'handler' => 'trello_views_backend_handler_field',
      'click sortable' => TRUE,
    ),
  );

  $data['trello']['id'] = array(
    'title' => t('Id'),
    'help' => t('ID of the item'),
    'field' => array(
      'handler' => 'trello_views_backend_handler_field',
      'click sortable' => TRUE,
    ),
  );

  $data['trello']['desc'] = array(
    'title' => t('Description'),
    'help' => t('Description of the item'),
    'field' => array(
      'handler' => 'trello_views_backend_handler_field',
      'click sortable' => TRUE,
    ),
  );

  $data['trello']['trello_id'] = array(
    'title' => t('Trello ID'),
    'help' => t('Set Trello ID'),
    'argument' => array(
      'handler' => 'trello_views_backend_handler_argument',
    ),
  );

  return $data;

}

/**
 * Implements hook_views_plugins().
 */
function trello_views_backend_views_plugins() {
  return array(
    'query' => array(
      'trello_views_backend' => array(
        'title' => t('Trello'),
        'help' => t('Queries Trello'),
        'handler' => 'trello_views_backend_plugin_query_trello',
      ),
    ),
  );
}
