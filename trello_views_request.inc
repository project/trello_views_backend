<?php

/**
 * Description
 */
class TrelloViewsRequest extends Trello {

  /**
   * @var string
   *   Endpoint path.
   */
  private $endpoint;

  /**
   * @var array
   *   Parsed result array for views.
   */
  private $parsedResult = array();

  /**
   * Set endpoint path.
   *
   * @param string $endpoint
   *   Endpoint path.
   *
   * @return TrelloViewsRequest
   *   TrelloViewsRequest object.
   */
  public function setEndpoint($endpoint) {
    $this->endpoint = $endpoint;
    return $this;
  }

  /**
   * Get endpoint path.
   *
   * @return string
   *   Endpoint path.
   */
  public function getEndpoint() {
    return $this->endpoint;
  }

  /**
   * Returns parsed result for views.
   *
   * @return array
   *   Parsed result for views.
   */
  public function getParsedResult() {
    return $this->parsedResult;
  }

  /**
   * Executes Trello query.
   */
  public function execute() {
    $this->get($this->getEndpoint());
    return $this;
  }

  /**
   * Parse Trello data for views.
   *
   * @return TrelloViewsRequest.
   *   TrelloViewsRequest object.
   */
  public function parse() {
    foreach ($this->result['data'] as $key => $value) {
      $this->parsedResult[$key] = $value;
    }
    return $this;
  }
}

