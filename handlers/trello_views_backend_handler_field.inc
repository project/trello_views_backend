<?php
/**
 * @file
 * Base field handler for trello_views_backend.
 */

class trello_views_backend_handler_field extends views_handler_field {

  function render_field($value) {
    return check_plain(decode_entities($value));
  }

  function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['trello_test'] = array(
      '#title' => t('Options for trello'),
      '#description' => t('Trololoo'),
      '#type' => 'checkbox',
      '#default_value' => $this->options['multiple'],
    );
  }
}

