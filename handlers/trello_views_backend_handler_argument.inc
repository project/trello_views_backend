<?php
/**
 * @file
 * Passthrough argument handler for trello_views_backend.
 */

class trello_views_backend_handler_argument extends views_handler_argument_string {

  /**
   * Override options_form() so that only the relevant options
   * are displayed to the user.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    unset($form['exception']);
  }

  /**
   * Override the behavior of query() to prevent the query
   * from being changed in any way.
   */
  function query($group_by = FALSE) {}

}
