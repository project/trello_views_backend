<?php

/**
 * @file
 * Query plugin for trello_views_backend.module.
 */
class trello_views_backend_plugin_query_trello extends views_plugin_query {

  function build(&$view) {
    parent::build($view);

    $trello = new TrelloViewsRequest();

    // Gather Trello request arguments.
    $endpoint = $view->query->options['endpoint'];
    $id = $view->argument['trello_id']->argument;
    $arguments = $view->query->options['arguments'];

    // Parse Trello request path.
    $path = '/' . $endpoint . '/' . $id . '/' . $arguments;

    // Set endpoint and execute Trello query.
    $trello->setEndpoint($path);
    $trello->execute();

    $this->request = $trello;
    $view->build_info['trello_views_backend_request'] = $this->request;

    // Create a new pager object.
    $view->init_pager();
    $this->pager->query();
  }

  function execute(&$view) {
    parent::execute($view);

    $request = $view->build_info['trello_views_backend_request'];
    $start = microtime(TRUE);

    // Pager settings.
    $this->request->pager_settings = $this->pager;

    // Get parsed results from TrelloViewsRequest object.
    $view->result = array();
    $view->result = $request->parse()->getParsedResult();

    // Tell pager and views total item count.
    $this->pager->total_items = count($view->result);
    $view->total_rows = $this->pager->total_items;
    $this->pager->update_page_info();

    $view->execute_time = microtime(TRUE) - $start;
  }

  function add_field($table, $field, $alias = '', $params = array()) {
    $alias = $field;

    // Add field info array.
    if (empty($this->fields[$field])) {
      $this->fields[$field] = array(
        'field' => $field,
        'table' => $table,
        'alias' => $alias,
      ) + $params;
    }

    return $field;
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['endpoint'] = array('default' => '');
    $options['arguments'] = array('default' => '');
    return $options;
  }

  function options_form(&$form, &$form_state) {
    $endpoint_options = array(
      'board' => 'Board',
      'organizations' => 'Organizations',
      'member' => 'Member',
    );

    $form['endpoint'] = array(
      '#type' => 'select',
      '#title' => t('Endpoint'),
      '#multiple' => FALSE,
      '#description' => t('Trello endpoint'),
      '#options' => $endpoint_options,
      '#default_value' => isset($this->options['endpoint']) ? $this->options['endpoint'] : -1,
      '#required' => TRUE,
    );

    $form['arguments'] = array(
      '#type' => 'textfield',
      '#title' => t('Arguments'),
      '#default_value' => $this->options['arguments'],
      '#description' => t("All the arguments that come after the id."),
      '#required' => TRUE,
    );
  }
}

